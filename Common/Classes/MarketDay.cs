﻿using System;

namespace Common.Classes
{
    //for use in schedule
    public sealed class MarketDay : ReportDay
    {
        public Int32 Duration { get; set; }
    }
}
