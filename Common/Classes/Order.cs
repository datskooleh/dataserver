﻿using Common.Enums;
using System;

namespace Common.Classes
{
    /// <summary>
    /// Order representation
    /// </summary>
    public class Order
    {
        /// <summary>
        /// Unique server order id
        /// </summary>
        public String Id { get; set; }

        /// <summary>
        /// Unique user custom id
        /// </summary>
        public String CustomId { get; set; }

        //public String Account { get { return "X"; } }

        /// <summary>
        /// Order symbol
        /// </summary>
        public String Symbol { get; set; }

        /// <summary>
        /// Order side
        /// </summary>
        public Side Side { get; set; }

        /// <summary>
        /// Order type
        /// </summary>
        public OrderType Type { get; set; }

        /// <summary>
        /// Trading order status
        /// </summary>
        public OrderStatus Status { get; set; }

        //public String Currency { get; set; }

        /// <summary>
        /// Qunatity to fill
        /// </summary>
        public Int32 Quantity { get; set; }

        /// <summary>
        /// Price. Not required if Side is Buy
        /// </summary>
        public Double Price { get; set; }

        /// <summary>
        /// Last filled quantity
        /// </summary>
        public Int32 LastQuatity { get; set; }

        /// <summary>
        /// Last price for filled items
        /// </summary>
        public Double LastPrice { get; set; }

        /// <summary>
        /// Remaining quantity open for execution
        /// </summary>
        public Int32 LeavesQuantity { get; set; }

        /// <summary>
        /// Timespamp of order request
        /// </summary>
        public DateTime TransactTime { get; set; }

        public override int GetHashCode()
        {
            var hash = 23;

            return hash * 7 * (!String.IsNullOrWhiteSpace(Id) ? Id.GetHashCode() : 1) +
                hash * 7 * (!String.IsNullOrWhiteSpace(CustomId) ? CustomId.GetHashCode() : 1) +
                hash * 7 * (!String.IsNullOrWhiteSpace(Symbol) ? Symbol.GetHashCode() : 1) +
                hash * 7 * Side.GetHashCode() +
                hash * 7 * Type.GetHashCode() +
                hash * 7 * Status.GetHashCode() +
                hash * 7 * Quantity.GetHashCode() +
                hash * 7 * Price.GetHashCode() +
                hash * 7 * LastQuatity.GetHashCode() +
                hash * 7 * LastPrice.GetHashCode() +
                hash * 7 * LeavesQuantity.GetHashCode() +
                hash * 7 * TransactTime.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var result = obj != null;

            if (result)
            {
                var order = obj as Order;

                result = order != null;

                if (result)
                    result = Equals(order);
            }

            return result;
        }

        private Boolean Equals(Order obj)
        {
            return obj.Id != null && obj.Id.Equals(Id);
        }
    }
}
