﻿using System;

namespace Common.Classes
{
    /// <summary>
    /// Represents single quote
    /// </summary>
    public class Quote
    {
        /// <summary>
        /// Quote symbol
        /// </summary>
        public String Symbol { get; set; }

        /// <summary>
        /// Last date quote updated
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Quote bid price
        /// </summary>
        public Double Bid { get; set; }

        /// <summary>
        /// Quote bid size
        /// </summary>
        public Double BidSize { get; set; }

        /// <summary>
        /// Quote ask price
        /// </summary>
        public Double Ask { get; set; }

        /// <summary>
        /// Quote ask size
        /// </summary>
        public Double AskSize { get; set; }

        /// <summary>
        /// Quote price calculated as (Bid + Ask) / 2
        /// </summary>
        public Double Price { get; set; }

        /// <summary>
        /// Quote volume calculated as BidSide + AskSize
        /// </summary>
        public Double Volume { get; set; }

        public Quote()
        {
            Symbol = String.Empty;
            Date = default(DateTime);
            Bid = default(Double);
            BidSize = default(Double);
            Ask = default(Double);
            AskSize = default(Double);
            Price = default(Double);
            Volume = default(Double);
        }
    }
}
