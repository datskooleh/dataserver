﻿using System;

namespace Common.Classes
{
    public class ReportDay
    {
        public String Start { get; set; }

        public String Day { get; set; }
    }
}
