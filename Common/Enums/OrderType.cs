﻿namespace Common.Enums
{
    /// <summary>
    /// Order type
    /// </summary>
    public enum OrderType
    {
        Market = 1,
        Limit
    }
}
