﻿namespace Common.Enums
{
    /// <summary>
    /// Order side
    /// </summary>
    public enum Side
    {
        Buy = 1,
        Sell
    }
}
