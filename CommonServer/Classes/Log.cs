﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CommonServer.Classes
{
    public static class Log
    {
        private static String _productName;
        private static String _rootPathForLog;

        static Log()
        {
            Assembly assembly = Assembly.GetEntryAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            _productName = fvi.ProductName;

            //_rootPathForLog = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), _productName);
            _rootPathForLog = Path.Combine(@"D:\Development\DataServer", _productName);
        }

        public static void WriteApplicationError(string ClassName, string MethodName, Exception Exception)
        {
            try
            {
                FileStream FileStream = new FileStream(Path.Combine(_rootPathForLog, DateTime.Now.ToString("yyyy_MM_dd") + "_ApplicationLog.txt"), FileMode.Append);
                StreamWriter Writer = new StreamWriter(FileStream);
                Writer.WriteLine(string.Format("Date: {0} ClassName: {1} MethodName: {2} Data: {3}", DateTime.Now, ClassName, MethodName, Exception.Message));
                Writer.Close();
                FileStream.Close();
            }
            catch { }
        }

        public static void WriteFixError(string ClassName, string MethodName, Exception Exception, QuickFix.Message message)
        {
            try
            {
                FileStream FileStream = new FileStream(Path.Combine(_rootPathForLog, DateTime.Now.ToString("yyyy_MM_dd") + "_FixLog.txt"), FileMode.Append);
                StreamWriter Writer = new StreamWriter(FileStream);
                Writer.WriteLine(string.Format("Date: {0} ClassName: {1} MethodName: {2} Data: {3} FixMessage: {4}", DateTime.Now, ClassName, MethodName, Exception.Message, (message != null ? message.ToString() : "")));
                Writer.Close();
                FileStream.Close();
            }
            catch { }
        }

        public static void WriteFixData(QuickFix.Message message)
        {
            //System.Diagnostics.Debug.WriteLine(message.ToString());
            try
            {
                FileStream FileStream = new FileStream(Path.Combine(Path.Combine(_rootPathForLog), DateTime.Now.ToString("yyyy_MM_dd") + "_Data.txt"), FileMode.Append);
                StreamWriter Writer = new StreamWriter(FileStream);
                Writer.WriteLine(DateTime.Now + "   " + message);
                Writer.Close();
                FileStream.Close();
            }
            catch { }
        }
    }
}
