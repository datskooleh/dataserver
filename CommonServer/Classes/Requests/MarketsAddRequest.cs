﻿using Common.Classes;

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CommonServer.Classes.Requests
{
    [DataContract]
    
    public sealed class MarketsAddRequest : Request
    {
        public MarketsAddRequest(String exchangeName, List<MarketDay> schedule)
        {
            ExchangeName = exchangeName;
            Schedule = schedule;
        }

        public MarketsAddRequest() : this(String.Empty, null)
        {
        }

        [DataMember]
        public String ExchangeName { get; set; }

        [DataMember]
        public List<MarketDay> Schedule { get; set; }
    }
}
