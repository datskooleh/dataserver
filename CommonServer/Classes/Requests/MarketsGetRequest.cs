﻿using System;
using System.Runtime.Serialization;

namespace CommonServer.Classes.Requests
{
    [DataContract]
    
    public sealed class MarketsGetRequest : Request
    {
        public MarketsGetRequest(String exchangeName)
        {
        }

        public MarketsGetRequest() : this(String.Empty)
        {
        }

        [DataMember]
        public String ExchangeName { get; set; }
    }
}
