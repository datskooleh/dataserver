﻿using Common.Classes;

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CommonServer.Classes.Requests
{
    [DataContract]
    
    public sealed class MarketsUpdateRequest : Request
    {
        public MarketsUpdateRequest(String exchangeName, List<MarketDay> schedule)
        {
            ExchangeName = exchangeName;
            Schedule = schedule;
        }

        [DataMember]
        public String ExchangeName { get; set; }

        [DataMember]
        public List<MarketDay> Schedule { get; set; }
    }
}
