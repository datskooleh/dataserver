﻿using System;
using System.Runtime.Serialization;

namespace CommonServer.Classes.Requests
{
    [DataContract]
    public class PlaceOrderRequest : Request
    {
        [DataMember]
        public String CustomId { get; set; }

        [DataMember]
        public String Symbol { get; set; }

        [DataMember]
        public Int32 Side { get; set; }

        [DataMember]
        public Int32 Type { get; set; }

        [DataMember]
        public Int32 Quantity { get; set; }

        [DataMember]
        public Double Price { get; set; }

        [DataMember]
        public DateTime TransactTime { get; set; }
    }
}
