﻿using System;
using System.Runtime.Serialization;

namespace CommonServer.Classes.Requests
{
    [DataContract]
    
    public sealed class ReportsGetRequest : Request
    {
        public ReportsGetRequest(String userLogin)
        {
        }

        public ReportsGetRequest() : this(String.Empty)
        {
        }

        [DataMember]
        public String UserLogin { get; set; }
    }
}
