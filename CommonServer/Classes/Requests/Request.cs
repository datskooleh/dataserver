﻿using CommonServer.Interfaces;

using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace CommonServer.Classes.Requests
{
    [DataContract]
    [KnownType(typeof(LoginRequest))]
    [KnownType(typeof(LogoutRequest))]
    [KnownType(typeof(MarketsAddRequest))]
    [KnownType(typeof(MarketsGetRequest))]
    [KnownType(typeof(MarketsUpdateRequest))]
    [KnownType(typeof(ReportAddRequest))]
    [KnownType(typeof(ReportDeleteRequest))]
    [KnownType(typeof(ReportsGetRequest))]
    [KnownType(typeof(ReportUpdateRequest))]
    [KnownType(typeof(UsersGetRequest))]
    [KnownType(typeof(UserUpdateRequest))]
    [KnownType(typeof(SubscribeRequest))]
    [KnownType(typeof(UnsubscribeRequest))]
    [KnownType(typeof(PlaceOrderRequest))]
    public class Request
    {
        /// <summary>
        /// identifies user/session info where the request received
        /// </summary>
        [IgnoreDataMember]
        public IUser User { get; set; }

        [JsonProperty]
        public string MsgType { get; set; }

        public Request()
        {
            this.MsgType = this.GetType().Name;
        }
    }
}
