﻿using System;
using System.Runtime.Serialization;

namespace CommonServer.Classes.Requests
{
    [DataContract]
    public class SubscribeRequest : Request
    {
        [DataMember]
        public String Symbol { get; set; }
    }
}
