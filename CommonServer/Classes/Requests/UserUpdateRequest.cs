﻿using System;
using System.Runtime.Serialization;

namespace CommonServer.Classes.Requests
{
    [DataContract]
    
    public sealed class UserUpdateRequest : Request
    {
        [DataMember]
        public String UserLogin { get; set; }

        [DataMember]
        public String UserMaxTradesNumber { get; set; } //int

        [DataMember]
        public String BalancePointToDollarRatio; //double

        [DataMember]
        public String BackstopPoints; // int;

        [DataMember]
        public String AutoCompoundStatus; //byte: 1 or 0

        [DataMember]
        public String AutoCompoundMultiptier; //double
    }
}
