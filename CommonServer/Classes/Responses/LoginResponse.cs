﻿using System;
using System.Runtime.Serialization;

namespace CommonServer.Classes.Responses
{
    [DataContract]
    public sealed class LoginResponse : Response
    {
        [DataMember]
        public String Message { get; set; }
    }
}
