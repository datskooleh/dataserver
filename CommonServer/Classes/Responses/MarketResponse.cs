﻿using System;
using System.Runtime.Serialization;

namespace CommonServer.Classes.Responses
{
    [DataContract]
    public sealed class MarketResponse : Response
    {
        //Add action: exhange already exist
        //Add action: invalid client id (if empty)
        //Add action: invalid ExchangeName
        //Add action: invalid Schedule data
        //Update action: invalid client id (if empty)
        //Update action: invalid ExchangeName
        //Update action: invalid Schedule data
        [DataMember]
        public String Message { get; set; }
    }
}
