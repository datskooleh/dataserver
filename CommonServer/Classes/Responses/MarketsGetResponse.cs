﻿using Common.Classes;

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CommonServer.Classes.Responses
{
    [DataContract]
    public sealed class MarketsGetResponse : Response
    {
        [DataMember]
        public Dictionary<String, List<MarketDay>> Markets { get; set; }

        [DataMember]
        public String Message { get; set; }
    }
}
