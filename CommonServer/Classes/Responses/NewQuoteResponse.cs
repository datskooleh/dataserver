﻿using System.Runtime.Serialization;

namespace CommonServer.Classes.Responses
{
    [DataContract]
    public class NewQuoteResponse : Response
    {
        [DataMember]
        public Common.Classes.Quote Quote { get; set; }
    }
}
