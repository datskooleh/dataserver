﻿using System;
using System.Runtime.Serialization;

namespace CommonServer.Classes.Responses
{
    [DataContract]
    public class PlaceOrderResponse : Response
    {
        public String Message { get; set; }
    }
}
