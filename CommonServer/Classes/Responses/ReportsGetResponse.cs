﻿using Common.Classes;

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CommonServer.Classes.Responses
{
    [DataContract]
    public sealed class ReportsGetResponse : Response
    {
        [DataMember]
        public Dictionary<String, List<ReportDay>> Reports { get; set; }

        [DataMember]
        public String Message { get; set; }
    }
}
