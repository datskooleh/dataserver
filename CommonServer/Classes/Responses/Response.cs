﻿using CommonServer.Interfaces;

using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace CommonServer.Classes.Responses
{
    [DataContract]
    [KnownType(typeof(LoginResponse))]
    [KnownType(typeof(LogoutResponse))]
    [KnownType(typeof(MarketResponse))]
    [KnownType(typeof(MarketsGetResponse))]
    [KnownType(typeof(ReportResponse))]
    [KnownType(typeof(ReportsGetResponse))]
    [KnownType(typeof(UserResponse))]
    [KnownType(typeof(UsersGetResponse))]
    [KnownType(typeof(NewQuoteResponse))]
    [KnownType(typeof(PlaceOrderResponse))]
    [KnownType(typeof(OrderHistoryResponse))]
    public class Response
    {
        /// <summary>
        /// identifies user/session info where the Response received
        /// </summary>
        [IgnoreDataMember]
        public IUser User { get; set; }

        [JsonProperty]
        public string MsgType { get; set; }

        public Response()
        {
            this.MsgType = this.GetType().Name;
        }
    }
}
