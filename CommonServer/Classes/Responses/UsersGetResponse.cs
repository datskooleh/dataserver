﻿using System;
using System.Runtime.Serialization;

namespace CommonServer.Classes.Responses
{
    [DataContract]
    public sealed class UsersGetResponse : Response
    {
        [DataMember]
        public String UserLogin { get; set; }

        [DataMember]
        public Int32 UserMaxTradesNumber { get; set; } //int

        [DataMember]
        public Double BalancePointToDollarRatio; //double

        [DataMember]
        public Int32 BackstopPoints; // int;

        [DataMember]
        public Byte AutoCompoundStatus; //byte: 1 or 0

        [DataMember]
        public Double AutoCompoundMultiptier; //double

        [DataMember]
        public String Message { get; set; }
    }
}
