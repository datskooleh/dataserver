﻿using CommonServer.Classes.Requests;
using CommonServer.Classes.Responses;
using CommonServer.Enums;
using CommonServer.Interfaces;
using Newtonsoft.Json;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Config;
using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CommonServer.Classes
{
    /// <summary>
    /// The base class for any server
    /// </summary>
    public abstract class ServerBase : IServer, IDisposable
    {
        #region Private members
        private WebSocketServer _server;
        #endregion //Private members

        #region Protected members
        protected SqlWorker _sqlWorker;
        protected abstract ServerType _serverType { get; }
        #endregion //Protected members

        #region Static variables
        /// <summary>
        /// Users collection by session Id
        /// </summary>
        private static Dictionary<String, IUser> Sessions;
        /// <summary>
        /// Method to be invoked for deserialization. Parsed on the static constructor.
        /// </summary>
        private static MethodInfo ReflectedDeserializationMethod;
        /// <summary>
        /// Available requests list that can be invoked by the server
        /// </summary>
        private static List<Type> Requests;
        #endregion //Static variables

        #region ctors
        public ServerBase()
        {
            _sqlWorker = new SqlWorker();
        }

        static ServerBase()
        {
            Sessions = new Dictionary<String, IUser>();

            ReflectedDeserializationMethod = typeof(JsonConvert).GetMethods()
                .First(methodInfo => methodInfo.Name
                    .Contains("DeserializeObject") && methodInfo.IsGenericMethod);

            try
            {
                var commonServerAssembly = Assembly.Load("CommonServer");

                if (commonServerAssembly != null)
                    Requests = commonServerAssembly
                        .GetTypes()
                        .Where(x => x.Namespace
                            .Contains("Requests")).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion //ctors

        #region Public methods
        /// <summary>
        /// Return true if server is started
        /// </summary>
        public bool Started
        {
            get { return _server != null && _server.State == SuperSocket.SocketBase.ServerState.Running; }
        }

        /// <summary>
        /// Starts the server
        /// </summary>
        /// <param name="args">Arguments that contains ip and port number to start server on</param>
        /// <returns></returns>
        public bool Start(Dictionary<String, String> args)
        {
            var started = true;
            try
            {
                _server = new WebSocketServer();

                var ip = args["ip"];
                var port = Convert.ToInt32(args["port"]);

                var config = new ServerConfig()
                {
                    Ip = ip,
                    Port = port,
                    MaxRequestLength = Int16.MaxValue,
                    MaxConnectionNumber = Int16.MaxValue,
                };

                started = _server.Setup(config) && _server.Start();

                if (started)
                {
                    _server.NewSessionConnected += new SessionHandler<WebSocketSession>(OnNewSessionConnected);
                    _server.SessionClosed += new SessionHandler<WebSocketSession, CloseReason>(OnSessionClosed);
                    _server.NewMessageReceived += new SessionHandler<WebSocketSession, String>(OnNewMessageReceived);
                }
            }
            catch (Exception ex)
            {
                started = false;
            }

            if (started)
            {
                try
                {
                    var connectionString = File.ReadAllText("DataBaseIAutentification.set");
                    if (!_sqlWorker.Init(connectionString))
                    {
                        Stop();
                        started = false;
                    }
                }
                catch
                {
                    started = false;
                }
            }

            return started;
        }

        /// <summary>
        /// Stop the server
        /// </summary>
        public void Stop()
        {
            _server.NewSessionConnected -= new SessionHandler<WebSocketSession>(OnNewSessionConnected);
            _server.SessionClosed -= new SessionHandler<WebSocketSession, CloseReason>(OnSessionClosed);
            _server.NewMessageReceived -= new SessionHandler<WebSocketSession, String>(OnNewMessageReceived);

            _server.Stop();
        }

        /// <summary>
        /// Return user if it exist
        /// </summary>
        /// <param name="sessionId">Id to search user</param>
        /// <returns></returns>
        public IUser GetUser(String sessionId)
        {
            lock (Sessions)
            {
                IUser user;
                Sessions.TryGetValue(sessionId, out user);

                return user;
            }
        }
        #endregion //Public methods

        #region WebSocket handlers
        private void OnNewSessionConnected(WebSocketSession args)
        {
            lock (Sessions)
            {
                if (!Sessions.ContainsKey(args.SessionID))
                    Sessions.Add(args.SessionID, null);

                //ToDo log new session connected here
            }
        }

        private void OnSessionClosed(WebSocketSession args, CloseReason e)
        {
            lock (Sessions)
            {
                IUser user = null;

                if (Sessions.TryGetValue(args.SessionID, out user))
                    Sessions.Remove(args.SessionID);
                else
                    return;

                if (SessionClosed != null)
                    SessionClosed(user);
            }
        }

        private void OnNewMessageReceived(WebSocketSession args, String message)
        {
            Request request = JsonConvert.DeserializeObject<Request>(message);
            var requestType = request.MsgType;

            if (request == null || String.IsNullOrWhiteSpace(request.MsgType) || !CheckRequestExist(request.MsgType))
                args.Send("Invalid request.");
            else if (request.MsgType == typeof(LoginRequest).Name)
            {
                var response = new LoginResponse();
                response.Message = String.Empty;

                try
                {
                    var deserializedRequest = JsonConvert.DeserializeObject<LoginRequest>(message);

                    Tuple<String, User> authenticationResult = null;

                    if (!String.IsNullOrWhiteSpace(deserializedRequest.Login) && !String.IsNullOrWhiteSpace(deserializedRequest.Password))
                        authenticationResult = _sqlWorker.UserAuthenticate(deserializedRequest.Login, deserializedRequest.Password);

                    var authenticated = false;

                    authenticated = authenticationResult != null
                        && String.IsNullOrWhiteSpace(authenticationResult.Item1);

                    if (authenticated)
                    {
                        var user = authenticationResult.Item2;
                        user.Session = args;
                        lock (Sessions)
                        {
                            //check if same user is logged in
                            var oldLoginSession = Sessions
                                .Select(collectionUser => collectionUser.Value)
                                .Where(collectionUser => collectionUser != null && collectionUser.Login
                                    .Equals(user.Login))
                                    .SingleOrDefault();

                            if (oldLoginSession != null) //this is error
                            {
                                var logoutResponse = new LogoutResponse();
                                logoutResponse.Message = "Another user try to login. Connect again please.";
                                (oldLoginSession.Session as WebSocketSession).Send(JsonConvert.SerializeObject(logoutResponse));
                                response.Message = "Another user is already logged in. Try again please.";

                                Sessions[oldLoginSession.SessionId].Dispose();
                                Sessions.Remove(args.SessionID);
                            }
                            else //this is good
                                Sessions[args.SessionID] = user;
                        }
                    }
                    else
                    {
                        if (authenticationResult != null)
                            response.Message = authenticationResult.Item1;
                        else
                            response.Message = "Invalid credentials.";
                    }
                }
                catch (Exception ex)
                {
                    response.Message = "Invalid login data.";
                }

                args.Send(JsonConvert.SerializeObject(response));
            }
            else if (request.MsgType == typeof(LogoutRequest).Name)
            {
                try
                {
                    var response = new LogoutResponse();

                    lock (Sessions)
                        if (Sessions.ContainsKey(args.SessionID))
                            Sessions[args.SessionID].Dispose();

                    response.Message = "Successfuly logged out.";

                    args.Send(JsonConvert.SerializeObject(response));
                }
                catch (Exception ex)
                {
                    var response = new LogoutResponse();
                    response.Message = "Invalid data";
                    args.Send(JsonConvert.SerializeObject(response));
                }
            }
            else
            {
                lock (Sessions)
                    if (Sessions[args.SessionID] == null)
                        args.Send("User must be logged in.");
                    else if (NewMessageReceived != null)
                    {
                        var deserializedMessage = Deserialize(requestType, message);
                        deserializedMessage.User = GetUser(args.SessionID);

                        System.Threading.Tasks.Task.Factory.StartNew(() => NewMessageReceived(deserializedMessage));
                    }
            }
        }
        #endregion //WebSocket handlers

        #region Private methods
        /// <summary>
        /// Check if Request exist in connection.
        /// </summary>
        /// <param name="messageType">Message type to be searched in the requests</param>
        /// <returns></returns>
        private Boolean CheckRequestExist(string messageType)
        {
            var typeExist = false;

            try
            {
                if (Requests != null && Requests.Count > 0)
                    typeExist = ServerBase.Requests
                        .Any(x => x.Name
                            .Equals(messageType));
            }
            catch
            {
                typeExist = false;
            }

            return typeExist;
        }

        /// <summary>
        /// Deserialize message depending on it's type.
        /// </summary>
        /// <param name="typeName">Type to be deserialized in.</param>
        /// <param name="message">Message to be deserialized.</param>
        /// <returns></returns>
        private Request Deserialize(String typeName, String message)
        {
            Type type = null;

            if (Requests != null && Requests.Count > 0)
                type = ServerBase.Requests
                    .SingleOrDefault(x => x.Name
                        .Equals(typeName));

            var genericMethod = ServerBase.ReflectedDeserializationMethod.MakeGenericMethod(type);

            var deserializationResult = genericMethod.Invoke(null, new object[] { message }) as Request;

            return deserializationResult;
        }
        #endregion //Private methods

        #region Events
        protected static Action<Request> NewMessageReceived;

        protected static Action<IUser> SessionClosed;
        #endregion //Events

        #region IDisposable Members
        public virtual void Dispose()
        {
            _server.Stop();
        }
        #endregion
    }
}
