﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace CommonServer.Classes
{
    public sealed class SqlWorker
    {
        private object locker = new object();

        private String _connectionString;

        private static List<String> _initializedServers;

        static SqlWorker()
        {
            _initializedServers = new List<String>();
        }

        public Boolean Init(String connectionString)
        {
            var isAllOk = true;

            var csb = new SqlConnectionStringBuilder(connectionString);
            lock (_initializedServers)
            {
                if (!_initializedServers.Contains(csb.DataSource))
                {
                    try
                    {
                        using (SqlConnection connection = new SqlConnection(csb.ConnectionString))
                        {
                            connection.Open();
                            connection.Close();
                            _connectionString = connectionString;
                        }

                        _initializedServers.Add(csb.DataSource);
                    }
                    catch
                    {
                        isAllOk = false;
                    }
                }
            }

            return isAllOk;
        }

        //Item1 contains message with error
        //Item2 is user - null if not authenticated
        public Tuple<String, User> UserAuthenticate(String login, String password)
        {
            var resultMessage = String.Empty;
            User user = null;

            using (var connection = new SqlConnection(_connectionString))
            {
                var connectionResult = Connect(connection);

                if (String.IsNullOrWhiteSpace(connectionResult))
                {
                    using (var cmd = connection.CreateCommand())
                    {
                        cmd.CommandText = "SELECT TOP(1) [Login], [IsAdmin] FROM [dbo].[Users] WHERE [Login]=@login AND [Password]=@password";
                        cmd.Parameters.AddWithValue("login", login);
                        cmd.Parameters.AddWithValue("password", password);

                        try
                        {
                            using (var reader = cmd.ExecuteReader())
                            {
                                if (!reader.HasRows) resultMessage = "Login or password is incorrect.";
                                else
                                {
                                    while (reader.Read())
                                    {
                                        user = new User(reader.GetString(0));
                                        user.IsAdmin = reader.GetBoolean(1);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            resultMessage = "Server failed to validate user.";
                        }
                    }
                }
            }

            return Tuple.Create<String, User>(resultMessage, user);
        }

        #region Private Methods
        private String Connect(SqlConnection connection)
        {
            var connectionResult = String.Empty;

            lock (locker)
            {

                try
                {
                    connection.Open();
                }
                catch (Exception ex)
                {
                    connectionResult = "Connection can't be established.";
                }
            }

            return connectionResult;
        }
        #endregion
    }
}
