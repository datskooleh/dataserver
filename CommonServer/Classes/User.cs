﻿using CommonServer.Interfaces;
using SuperWebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonServer.Classes
{
    public sealed class User : IUser
    {
        private System.Threading.Timer _timer;

        public User(String login, WebSocketSession session)
        {
            this.Login = login;
            _session = session;

            _timer = new System.Threading.Timer(Heartbeat, null, 3000, 3000);
        }

        public User(String login)
        {
            this.Login = login;
        }

        WebSocketSession _session;
        public object Session
        {
            set
            {
                if (_session == null && value != null && value is WebSocketSession)
                {
                    _session = value as WebSocketSession;
                    _timer = new System.Threading.Timer(Heartbeat, null, 3000, 3000);
                }
            }
            get
            {
                return _session;
            }
        }

        public String SessionId
        {
            get
            {
                var sessionId = String.Empty;
                if (_session != null)
                    sessionId = _session.SessionID;

                return sessionId;
            }
        }

        public String Login { get; set; }

        public Boolean IsAdmin { get; set; }

        private void Heartbeat(object obj)
        {
        }

        public override Boolean Equals(object obj)
        {
            var result = false;

            result = obj != null;

            if (result)
            {
                var o = obj as User;

                result = o != null;

                if (result)
                    result = Equals(o);
            }

            return result;
        }

        public override Int32 GetHashCode()
        {
            Int32 hash = 13;
            return (hash * 7) +
                7 * SessionId.GetHashCode() +
                7 * Login.GetHashCode() +
                7 * IsAdmin.GetHashCode() +
                (_session == null ? 0 : _session.GetHashCode());
        }

        private Boolean Equals(User o)
        {
            var result = false;

            if (!String.IsNullOrWhiteSpace(this.Login)
                && !String.IsNullOrWhiteSpace(o.Login))
            {
                result = this.Login.Equals(o.Login);
            }

            return result;
        }

        public void SendMessage(String message)
        {
            _session.Send(message);
        }

        #region IDisposable Members
        public void Dispose()
        {
            _session.Close();
        }
        #endregion
    }
}
