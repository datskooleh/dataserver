﻿using System;
using System.Collections.Generic;

namespace CommonServer.Interfaces
{
    /// <summary>
    /// Interface for server
    /// </summary>
    public interface IServer
    {
        /// <summary>
        /// Return true if server is started
        /// </summary>
        Boolean Started { get; }

        /// <summary>
        /// Starts the server
        /// </summary>
        /// <param name="args">Arguments that contains ip and port number to start server on</param>
        /// <returns></returns>
        Boolean Start(Dictionary<String, String> args);

        /// <summary>
        /// Stop the server
        /// </summary>
        void Stop();

        /// <summary>
        /// Return user if it exist
        /// </summary>
        /// <param name="sessionId">Id to search user</param>
        /// <returns></returns>
        IUser GetUser(String sessionId);
    }
}
