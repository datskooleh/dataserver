﻿using System;

namespace CommonServer.Interfaces
{
    /// <summary>
    /// Represent server user
    /// </summary>
    public interface IUser : IDisposable
    {
        /// <summary>
        /// Session Id for user that was connected
        /// </summary>
        String SessionId { get; }

        /// <summary>
        /// User login
        /// </summary>
        String Login { get; set; }

        /// <summary>
        /// Is true when user admin
        /// </summary>
        Boolean IsAdmin { get; set; }

        /// <summary>
        /// Contains session object
        /// </summary>
        object Session { get; }

        /// <summary>
        /// Sends message to the user
        /// </summary>
        /// <param name="message">Message to be sent to the user</param>
        void SendMessage(String message);
    }
}
