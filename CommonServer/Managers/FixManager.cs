﻿using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX44;
using QuickFix.Transport;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CommonServer.Managers
{
    public class FixManager : MessageCracker, QuickFix.IApplication, IDisposable
    {
        #region Private members
        private Dictionary<String, SessionID> _sessions;
        private SocketInitiator _socketInitiator = null;
        private List<String> _symbols;
        private Dictionary<String, String> _subscribedSymbols;
        #endregion //Private members

        #region Events
        public delegate void NewQuoteHandler(Common.Classes.Quote quote);
        public event NewQuoteHandler OnNewQuote;

        public delegate void OrderExecutionReport(Common.Classes.Order order);
        public event OrderExecutionReport OnOrderExecutionReport;
        #endregion //Events

        #region Simulator
        private Random rand = new Random();
        private System.Threading.Timer _timer;

        private void SimulatorMethod(object state)
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                var quote = new Common.Classes.Quote();
                quote.Symbol = _symbols[rand.Next(0, _symbols.Count - 1)];
                quote.Date = DateTime.Now.ToUniversalTime();
                quote.Ask = rand.NextDouble();
                quote.AskSize = rand.Next(0, 100000);
                quote.Bid = rand.NextDouble();
                quote.BidSize = rand.Next(0, 100000);
                quote.Volume = quote.AskSize + quote.BidSize;
                quote.Price = (quote.Ask + quote.Bid) / 2;

                lock (OnNewQuote)
                    if (OnNewQuote != null) OnNewQuote(quote);
            });

            _timer.Change(rand.Next(0, 1000), rand.Next(0, 500));
        }
        #endregion //Simulator

        #region ctors
        public FixManager()
        {
            #region Simulator
            _timer = new System.Threading.Timer(SimulatorMethod, null, 0, 1000);
            #endregion //Simulator

            _sessions = new Dictionary<String, SessionID>();

            _subscribedSymbols = new Dictionary<String, String>();
            _symbols = new List<String>()
            {
                "EUR/USD",
                //"UAH/USD",
                //"CAD/USD",
                //"CHF/USD",
                //"GBP/USD",
                //"HRK/USD",
                //"LVL/USD",
                //"LTL/USD",
            };
        }
        #endregion //ctors

        #region Connection
        public void Connect()
        {
            QuickFix.SessionSettings settings = new QuickFix.SessionSettings(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "settings.cfg"));
            _socketInitiator = new QuickFix.Transport.SocketInitiator(this, new FileStoreFactory(settings), settings);
            _socketInitiator.Start();
        }

        public void Disconnect()
        {
            if (_socketInitiator != null)
            {
                _socketInitiator.Stop(true);
            }
        }
        #endregion //Connection

        #region IApplication implementation
        public void ToAdmin(global::QuickFix.Message message, SessionID sessionID)
        {
            CommonServer.Classes.Log.WriteFixData(message);
            try
            {
                Crack(message, sessionID);
            }
            catch (Exception ex) { CommonServer.Classes.Log.WriteFixError(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, ex, message); }
        }

        public void FromAdmin(global::QuickFix.Message message, SessionID sessionID)
        {
            CommonServer.Classes.Log.WriteFixData(message);
            try
            {
                Crack(message, sessionID);
            }
            catch (Exception ex) { CommonServer.Classes.Log.WriteFixError(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, ex, message); }
        }

        public void ToApp(global::QuickFix.Message message, SessionID sessionID)
        {
            CommonServer.Classes.Log.WriteFixData(message);
            try
            {
                Crack(message, sessionID);
            }
            catch (Exception ex) { CommonServer.Classes.Log.WriteFixError(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, ex, message); }
        }

        public void FromApp(global::QuickFix.Message message, SessionID sessionID)
        {
            CommonServer.Classes.Log.WriteFixData(message);
            try
            {
                Crack(message, sessionID);
            }
            catch (Exception ex) { CommonServer.Classes.Log.WriteFixError(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, ex, message); }
        }

        void IApplication.OnCreate(QuickFix.SessionID sessionID)
        {
            _sessions.Add(sessionID.SessionQualifier, sessionID);
        }

        void IApplication.OnLogon(QuickFix.SessionID sessionID)
        {
            if (sessionID.SessionQualifier == "Quote")
            {
                var uniqQuoteId = 0;
                _symbols.ForEach(symbol =>
                {
                    lock (_subscribedSymbols)
                    {
                        MarketDataRequest request = new MarketDataRequest();
                        request.Set(new MDReqID(uniqQuoteId.ToString()));
                        request.Set(new SubscriptionRequestType('1'));
                        request.Set(new MarketDepth(0));

                        MarketDataRequest.NoRelatedSymGroup relatedSymGroup = new MarketDataRequest.NoRelatedSymGroup();
                        relatedSymGroup.SetField(new NoRelatedSym(1));
                        relatedSymGroup.Set(new Symbol(symbol));
                        request.AddGroup(relatedSymGroup);

                        QuickFix.Session.SendToTarget(request, _sessions["Quote"]);

                        _subscribedSymbols.Add(uniqQuoteId.ToString(), symbol);
                    }
                    ++uniqQuoteId;
                });
            }
            else if (sessionID.SessionQualifier == "Trade")
            {
                NewOrderSingle newOrderSingle = new NewOrderSingle();
                newOrderSingle.SetField(new ClOrdID(Guid.NewGuid().ToString()));
                //newOrderSingle.SetField(new Account(OrderRequest.CustomID));
                newOrderSingle.SetField(new Symbol("EUR/USD"));
                newOrderSingle.SetField(new Currency("EUR"));
                newOrderSingle.SetField(new Side('1'));
                newOrderSingle.SetField(new Account("X"));
                newOrderSingle.SetField(new OrderQty(1000));
                newOrderSingle.SetField(new OrdType('1'));
                newOrderSingle.SetField(new Price((decimal)1.2));
                newOrderSingle.Set(new Deviation((decimal)0.0));
                newOrderSingle.Set(new TTL(-1));
                newOrderSingle.SetField(new MinQty(1));
                newOrderSingle.SetField(new TransactTime(DateTime.UtcNow));

                QuickFix.Session.SendToTarget(newOrderSingle, _sessions["Trade"]);
            }
        }

        public void OnLogout(SessionID sessionID)
        {
            lock (_subscribedSymbols)
                _subscribedSymbols.Clear();
        }
        #endregion //IApplication implementation

        /*public void OnLogon(SessionID sessionID)
        {
            symbols.ForEach(item =>
            {
                MarketDataRequest request = new MarketDataRequest();
                request.Set(new MDReqID("123"));
                request.Set(new SubscriptionRequestType('1'));
                request.Set(new MarketDepth(0));

                MarketDataRequest.NoRelatedSymGroup relatedSymGroup = new MarketDataRequest.NoRelatedSymGroup();
                relatedSymGroup.SetField(new NoRelatedSym(1));
                relatedSymGroup.Set(new Symbol(item));
                request.AddGroup(relatedSymGroup);

                Session.SendToTarget(request, _sessionID);
            });

            return;
            NewOrderSingle newOrderSingle = new NewOrderSingle();
            newOrderSingle.SetField(new ClOrdID(Guid.NewGuid().ToString()));
            //newOrderSingle.SetField(new Account(OrderRequest.CustomID));
            newOrderSingle.SetField(new Symbol("EUR/USD"));
            newOrderSingle.SetField(new Currency("EUR"));
            newOrderSingle.SetField(new Side('1'));
            newOrderSingle.SetField(new Account("X"));
            newOrderSingle.SetField(new OrderQty(1000));
            newOrderSingle.SetField(new OrdType('1'));
            newOrderSingle.SetField(new Price((decimal)1.2));
            newOrderSingle.Set(new Deviation((decimal)0.0));
            newOrderSingle.Set(new TTL(-1));
            newOrderSingle.SetField(new MinQty(1));
            newOrderSingle.SetField(new TransactTime(DateTime.UtcNow));

            //switch (OrderRequest.Type)
            //{            
            //    case Order.eType.Market:
            //        newOrderSingle.SetField(new OrdType('C'));
            //        break;           
            //    case Order.eType.Limit:
            //        newOrderSingle.SetField(new OrdType('F'));
            //        newOrderSingle.SetField(new Price((decimal)OrderRequest.LimitPrice));
            //        break;           
            //}
            //newOrderSingle.SetField(new Product(4));
            //newOrderSingle.Set(new Currency(OrderRequest.Symbol.LastIndexOf('/') > 0 ? OrderRequest.Symbol.Substring(0, OrderRequest.Symbol.LastIndexOf('/')) : ""));
            //switch (OrderRequest.TimeInForce)
            //{            
            //    case Order.eTimeInForce.GTC:
            //        newOrderSingle.SetField(new TimeInForce('1'));
            //        break;

            //    case Order.eTimeInForce.GTD:
            //        newOrderSingle.SetField(new TimeInForce('6'));
            //        newOrderSingle.SetField(new ExpireTime(OrderRequest.ExpirationTime.Value));
            //        break;
            //}
            Session.SendToTarget(newOrderSingle, _sessionID);
        }*/

        #region Messages
        public void OnMessage(Logon Message, SessionID SessionID)
        {
            if (!(Message.Header.IsSetField(Tags.TargetCompID) && Message.Header.IsSetField(Tags.SenderCompID) && Message.Header.GetString(Tags.TargetCompID) == SessionID.SenderCompID))
            {
                if (SessionID.SessionQualifier == "Quote")
                {
                    Message.SetField(new StringField(Tags.Username, "primexm_fix_uat_1_q"));
                    Message.SetField(new StringField(Tags.Password, "EfeRtMy8Q29MkUq8"));
                }
                else if (SessionID.SessionQualifier == "Trade")
                {
                    Message.SetField(new StringField(Tags.Username, "primexm_fix_uat_1_t"));
                    Message.SetField(new StringField(Tags.Password, "pmzEMKV5kiTjkXcC"));
                }
                //var sendingTime = Message.Header.GetDateTime(Tags.SendingTime);
                //System.TimeSpan tmSpan = System.DateTime.UtcNow - sendingTime;                
            }
        }

        public void OnMessage(Logout message, SessionID sessionID)
        {
            if (message.IsSetText())
            {
                //MessageBox.Show(message.Text.Obj);
            }
        }

        public void OnMessage(NewOrderSingle message, SessionID sessionID)
        {

        }

        public void OnMessage(MarketDataRequest message, SessionID sessionID)
        {

        }

        public void OnMessage(MarketDataRequestReject message, SessionID sessionID)
        {

        }

        public void OnMessage(MassQuote message, SessionID sessionID)
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                int quotesInMessage = message.GetInt(Tags.NoQuoteSets);
                for (int g = 1; g <= quotesInMessage; g++)
                {
                    Group group = message.GetGroup(g, Tags.NoQuoteSets);
                    Common.Classes.Quote quote = null;

                    lock (_subscribedSymbols)
                    {
                        var symbol = String.Empty;
                        if (_subscribedSymbols.TryGetValue(group.GetString(Tags.QuoteSetID), out symbol))
                        {
                            quote = new Common.Classes.Quote()
                            {
                                Symbol = symbol,
                                Date = message.Header.GetDateTime(Tags.SendingTime),
                            };
                        }
                        else
                            continue;
                    }
                    if (group.IsSetField(Tags.OfferSize))
                        quote.AskSize = group.GetInt(Tags.OfferSize);
                    if (group.IsSetField(Tags.BidSize))
                        quote.BidSize = group.GetInt(Tags.BidSize);
                    if (group.IsSetField(Tags.BidSpotRate))
                        quote.Bid = group.GetInt(Tags.BidSpotRate);
                    if (group.IsSetField(Tags.OfferSpotRate))
                        quote.Ask = group.GetInt(Tags.OfferSpotRate);

                    quote.Price = (quote.Ask + quote.Bid) / 2;
                    quote.Volume = quote.BidSize + quote.AskSize;

                    lock (OnNewQuote)
                        if (OnNewQuote != null) OnNewQuote(quote);
                }
            });
        }

        public void OnMessage(Heartbeat message, SessionID sessionID)
        {

        }

        public void OnMessage(ExecutionReport message, SessionID SessionID)
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                var order = new Common.Classes.Order();

                order.Id = message.GetString(Tags.ExecID);
                order.CustomId = message.GetString(Tags.ClOrdID);
                order.Symbol = message.GetString(Tags.Symbol);
                order.Quantity = message.GetInt(Tags.OrderQty);
                order.TransactTime = message.GetDateTime(Tags.TransactTime);

                if (message.IsSetField(Tags.Price))
                    order.Price = (Double)message.GetDecimal(Tags.Price);

                if (message.IsSetField(Tags.LastQty))
                    order.LastQuatity = message.GetInt(Tags.LastQty);

                if (message.IsSetField(Tags.LastPx))
                    order.LastPrice = (Double)message.GetDecimal(Tags.Price);

                if (message.IsSetField(Tags.LeavesQty))
                    order.LeavesQuantity = message.GetInt(Tags.LeavesQty);

                Common.Enums.Side side;
                var messageSide = message.GetString(Tags.Side);
                if (!Enum.TryParse<Common.Enums.Side>(messageSide, out side))
                    throw new FieldNotFoundException("No field with value \"" + messageSide + "\" in enum");
                else
                    order.Side = side;

                var orderStatusMessage = message.GetString(Tags.OrdStatus);
                Common.Enums.OrderStatus status;
                if (Enum.TryParse<Common.Enums.OrderStatus>(orderStatusMessage, out status))
                    order.Status = status;
                else
                    throw new FieldNotFoundException("No field with value \"" + orderStatusMessage + "\" in enum");

                var orderTypeMessage = message.GetString(Tags.OrdType);
                Common.Enums.OrderType type;
                if (Enum.TryParse<Common.Enums.OrderType>(orderTypeMessage, out type))
                    order.Type = type;
                else
                    throw new FieldNotFoundException("No field with value \"" + orderTypeMessage + "\" in enum");

                lock (OnOrderExecutionReport)
                    if (OnOrderExecutionReport != null) OnOrderExecutionReport(order);
            });
        }

        public void OnMessage(Reject message, SessionID sessionID)
        {

        }

        public void OnMessage(TradeCaptureReport message, SessionID sessionID)
        {
            try
            {
                if (message.IsSetField(Tags.Symbol) && message.IsSetField(Tags.TransactTime) && message.IsSetField(Tags.LastPx) && message.IsSetField(Tags.LastQty))
                {
                    Common.Classes.Quote quote = new Common.Classes.Quote() { Symbol = message.GetString(Tags.Symbol), Date = message.GetDateTime(Tags.TransactTime), Price = (double)message.GetDecimal(Tags.LastPx), Volume = message.GetInt(Tags.LastQty) };

                    lock (OnNewQuote)
                        if (OnNewQuote != null) OnNewQuote(quote);
                }
            }
            catch { }
        }
        #endregion //Messages

        public void PlaceOrder(Common.Classes.Order order)
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                NewOrderSingle newOrderSingle = new NewOrderSingle();
                newOrderSingle.SetField(new Account("X"));
                newOrderSingle.SetField(new ClOrdID(order.CustomId));
                newOrderSingle.SetField(new Symbol(order.Symbol));
                newOrderSingle.SetField(new Side(order.Side.ToString().First()));
                newOrderSingle.SetField(new OrderQty(order.Quantity));
                newOrderSingle.SetField(new OrdType(order.Type.ToString().First()));
                newOrderSingle.SetField(new TransactTime(DateTime.UtcNow));

                if (order.Type == Common.Enums.OrderType.Limit && 0 > order.Price)
                    throw new InvalidOperationException("Price cannot be less than 0.");
                else
                    newOrderSingle.SetField(new Price((Decimal)order.Price));

                lock (_sessions)
                    Session.SendToTarget(newOrderSingle, _sessions["Trade"]);
            });
        }

        #region IDisposable Members
        public void Dispose()
        {
            _socketInitiator.Stop();

            lock (_sessions)
                _sessions.Clear();

            lock (_symbols)
                _symbols.Clear();

            lock (_subscribedSymbols)
                _subscribedSymbols.Clear();
        }
        #endregion
    }
}
