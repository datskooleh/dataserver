﻿namespace ServerUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUserServer = new System.Windows.Forms.Button();
            this.btnAdminServer = new System.Windows.Forms.Button();
            this.lblAdminStatus = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbAdminServerIP = new System.Windows.Forms.TextBox();
            this.tbUserServerIP = new System.Windows.Forms.TextBox();
            this.tbUserServerPort = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbAdminServerPort = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblUserStatus = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnUserServer
            // 
            this.btnUserServer.Location = new System.Drawing.Point(6, 19);
            this.btnUserServer.Name = "btnUserServer";
            this.btnUserServer.Size = new System.Drawing.Size(75, 23);
            this.btnUserServer.TabIndex = 0;
            this.btnUserServer.Text = "Start";
            this.btnUserServer.UseVisualStyleBackColor = true;
            this.btnUserServer.Click += new System.EventHandler(this.btnUserServer_Click);
            // 
            // btnAdminServer
            // 
            this.btnAdminServer.Location = new System.Drawing.Point(9, 19);
            this.btnAdminServer.Name = "btnAdminServer";
            this.btnAdminServer.Size = new System.Drawing.Size(75, 23);
            this.btnAdminServer.TabIndex = 1;
            this.btnAdminServer.Text = "Start";
            this.btnAdminServer.UseVisualStyleBackColor = true;
            this.btnAdminServer.Click += new System.EventHandler(this.btnAdminServer_Click);
            // 
            // lblAdminStatus
            // 
            this.lblAdminStatus.AutoSize = true;
            this.lblAdminStatus.Location = new System.Drawing.Point(90, 24);
            this.lblAdminStatus.Name = "lblAdminStatus";
            this.lblAdminStatus.Size = new System.Drawing.Size(47, 13);
            this.lblAdminStatus.TabIndex = 3;
            this.lblAdminStatus.Text = "Stopped";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "IP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "IP";
            // 
            // tbAdminServerIP
            // 
            this.tbAdminServerIP.Location = new System.Drawing.Point(29, 48);
            this.tbAdminServerIP.MaxLength = 15;
            this.tbAdminServerIP.Name = "tbAdminServerIP";
            this.tbAdminServerIP.Size = new System.Drawing.Size(90, 20);
            this.tbAdminServerIP.TabIndex = 6;
            this.tbAdminServerIP.Text = "127.0.0.1";
            // 
            // tbUserServerIP
            // 
            this.tbUserServerIP.Location = new System.Drawing.Point(26, 48);
            this.tbUserServerIP.MaxLength = 15;
            this.tbUserServerIP.Name = "tbUserServerIP";
            this.tbUserServerIP.Size = new System.Drawing.Size(90, 20);
            this.tbUserServerIP.TabIndex = 7;
            this.tbUserServerIP.Text = "127.0.0.1";
            // 
            // tbUserServerPort
            // 
            this.tbUserServerPort.Location = new System.Drawing.Point(76, 74);
            this.tbUserServerPort.MaxLength = 5;
            this.tbUserServerPort.Name = "tbUserServerPort";
            this.tbUserServerPort.Size = new System.Drawing.Size(40, 20);
            this.tbUserServerPort.TabIndex = 9;
            this.tbUserServerPort.Text = "2012";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Port";
            // 
            // tbAdminServerPort
            // 
            this.tbAdminServerPort.Location = new System.Drawing.Point(79, 74);
            this.tbAdminServerPort.MaxLength = 5;
            this.tbAdminServerPort.Name = "tbAdminServerPort";
            this.tbAdminServerPort.Size = new System.Drawing.Size(40, 20);
            this.tbAdminServerPort.TabIndex = 11;
            this.tbAdminServerPort.Text = "2013";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Port";
            // 
            // lblUserStatus
            // 
            this.lblUserStatus.AutoSize = true;
            this.lblUserStatus.Location = new System.Drawing.Point(87, 24);
            this.lblUserStatus.Name = "lblUserStatus";
            this.lblUserStatus.Size = new System.Drawing.Size(47, 13);
            this.lblUserStatus.TabIndex = 12;
            this.lblUserStatus.Text = "Stopped";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.btnUserServer);
            this.groupBox1.Controls.Add(this.lblUserStatus);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbUserServerIP);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbUserServerPort);
            this.groupBox1.Location = new System.Drawing.Point(4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(135, 104);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Users server";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.lblAdminStatus);
            this.groupBox2.Controls.Add(this.btnAdminServer);
            this.groupBox2.Controls.Add(this.tbAdminServerPort);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.tbAdminServerIP);
            this.groupBox2.Location = new System.Drawing.Point(145, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(136, 104);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Admins server";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(285, 111);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnUserServer;
        private System.Windows.Forms.Button btnAdminServer;
        private System.Windows.Forms.Label lblAdminStatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbAdminServerIP;
        private System.Windows.Forms.TextBox tbUserServerIP;
        private System.Windows.Forms.TextBox tbUserServerPort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbAdminServerPort;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblUserStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

