﻿using CommonServer.Interfaces;

using Servers;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ServerUI
{
    public partial class Form1 : Form
    {
        Dictionary<string, IServer> servers = new Dictionary<string, IServer>();
        Dictionary<string, string> serverConfigurations = new Dictionary<string, string>();

        public Form1()
        {
            InitializeComponent();

            try
            {
                servers.Add("admin", new AdminServer());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can't load settings for server", "Admins server", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                btnAdminServer.Enabled = false;
            }

            try
            {
                servers.Add("user", new UserServer());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can't load settings for server", "Users server", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                btnUserServer.Enabled = false;
            }

            serverConfigurations.Add("ip", "");
            serverConfigurations.Add("port", (-1).ToString());
        }

        private void btnAdminServer_Click(object sender, EventArgs e)
        {
            if(ValidateConfiguration(tbAdminServerIP.Text, tbAdminServerPort.Text))
            {
                IServer server = servers["admin"];

                var result = StartStopServer(server, tbAdminServerIP.Text, tbAdminServerPort.Text);

                btnAdminServer.Text = result.Item1;
                lblAdminStatus.Text = result.Item2;

                if (btnAdminServer.Text.Contains("Start"))
                {
                    tbAdminServerIP.Enabled = true;
                    tbAdminServerPort.Enabled = true;
                }
                else
                {
                    tbAdminServerIP.Enabled = false;
                    tbAdminServerPort.Enabled = false;
                }
            }
        }

        private void btnUserServer_Click(object sender, EventArgs e)
        {
            if (ValidateConfiguration(tbUserServerIP.Text, tbUserServerPort.Text))
            {
                IServer server = servers["user"];

                var result = StartStopServer(server, tbUserServerIP.Text, tbUserServerPort.Text);

                btnUserServer.Text = result.Item1;
                lblUserStatus.Text = result.Item2;

                if (btnUserServer.Text.Contains("Start"))
                {
                    tbUserServerIP.Enabled = true;
                    tbUserServerPort.Enabled = true;
                }
                else
                {
                    tbUserServerIP.Enabled = false;
                    tbUserServerPort.Enabled = false;
                }
            }
        }

        //Item1 contains button text and Item2 contains status text
        private Tuple<string, string> StartStopServer(IServer server, String ip, String port)
        {
            var buttonText = String.Empty;
            var statusText = String.Empty;

            if (server.Started)
            {
                server.Stop();
                statusText = "Stopped";
                buttonText = "Start";
                ResetServerConfigurations();
            }
            else
            {
                serverConfigurations["ip"] = ip;
                serverConfigurations["port"] = port;

                if (server.Start(serverConfigurations))
                {
                    statusText = "Started";
                    buttonText = "Stop";

                    ResetServerConfigurations();
                }
                else
                {
                    statusText = "Stopped";
                    buttonText = "Start";
                }
            }

            return Tuple.Create<string, string>(buttonText, statusText);
        }

        private void ResetServerConfigurations()
        {
            serverConfigurations["ip"] = String.Empty;
            serverConfigurations["port"] = (-1).ToString();
        }

        private bool ValidateConfiguration(string ip, string port)
        {
            var isValid = false;

            var parsRes = 0;

            if (String.IsNullOrWhiteSpace(ip))
                MessageBox.Show("IP cannot be empty");
            else if (String.IsNullOrWhiteSpace(port))
                MessageBox.Show("Port cannot be empty");
            else if (!int.TryParse(port, out parsRes) || parsRes < 0)
                MessageBox.Show("Port is not valid");
            else
                isValid = true;

            return isValid;
        }
    }
}
