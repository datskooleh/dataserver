﻿using CommonServer.Classes;
using CommonServer.Classes.Requests;
using CommonServer.Classes.Responses;
using CommonServer.Enums;

using Newtonsoft.Json;
using SuperWebSocket;
using System;

namespace Servers
{
    public sealed class AdminServer : ServerBase
    {
        private object locker = new object();

        protected override ServerType _serverType { get { return ServerType.Admin; } }

        public AdminServer()
        {
            ServerBase.NewMessageReceived += OnNewMessageReceived;
        }

        private void OnMarketsAddRequest()
        {
        }

        private void OnNewMessageReceived(Request request)
        {
            var session = request.User.Session as WebSocketSession;

            lock (locker)
            {
                if (request is MarketsAddRequest)
                {
                    if (!request.User.IsAdmin)
                    {
                        var response = new MarketResponse();
                        response.Message = "Has no rights.";
                        session.Send(JsonConvert.SerializeObject(response));
                        return;
                    }
                }
                else if (request is MarketsUpdateRequest)
                { }
                else if (request is MarketsGetRequest)
                { }
                else if (request is ReportAddRequest)
                { }
                else if (request is ReportDeleteRequest)
                { }
                else if (request is ReportsGetRequest)
                { }
                else if (request is ReportUpdateRequest)
                { }
                else if (request is UsersGetRequest)
                { }
                else if (request is UserUpdateRequest)
                { }
            }
        }

        #region IDisposable Members
        public override void Dispose()
        {
            ServerBase.NewMessageReceived -= OnNewMessageReceived;

            base.Dispose();
        }
        #endregion
    }
}
