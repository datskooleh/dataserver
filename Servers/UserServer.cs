﻿using Common.Classes;
using CommonServer.Classes;
using CommonServer.Classes.Requests;
using CommonServer.Classes.Responses;
using CommonServer.Enums;
using CommonServer.Interfaces;
using CommonServer.Managers;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Servers
{
    public sealed class UserServer : ServerBase
    {
        #region Private members
        private FixManager _fixManager;
        private Dictionary<String, List<IUser>> _subscribedSymbols;
        private Dictionary<Order, IUser> _usersOrders;
        #endregion //Private members

        #region Protected members
        protected override ServerType _serverType { get { return ServerType.User; } }
        #endregion //Protected members

        #region ctors
        public UserServer()
        {
            ServerBase.NewMessageReceived += OnNewMessageReceived;
            ServerBase.SessionClosed += OnSessionClosed;

            _subscribedSymbols = new Dictionary<String, List<IUser>>();
            _usersOrders = new Dictionary<Order, IUser>();

            _fixManager = new FixManager();
            _fixManager.OnNewQuote += OnNewQuote;
            _fixManager.OnOrderExecutionReport += OnOrderHistory;
            _fixManager.Connect();
        }
        #endregion //ctors

        #region Event handlers
        private void OnNewMessageReceived(Request request)
        {
            if (request is SubscribeRequest) HandleSubscribeRequest(request as SubscribeRequest);
            else if (request is UnsubscribeRequest) HandleUnsubscribeRequest(request as UnsubscribeRequest);
            else if (request is PlaceOrderRequest) HandlePlaceOrderRequest(request as PlaceOrderRequest);
        }

        private void OnSessionClosed(IUser user)
        {
            lock (_subscribedSymbols)
            {
                var symbolsToRemove = new List<String>(_subscribedSymbols.Count / 4);

                foreach (var keyValuePair in _subscribedSymbols)
                {
                    var subscribedUsers = keyValuePair.Value;

                    subscribedUsers.Remove(user);

                    if (subscribedUsers.Count == 0)
                        symbolsToRemove.Add(keyValuePair.Key);
                }

                symbolsToRemove.ForEach(symbol =>
                {
                    _subscribedSymbols.Remove(symbol);
                });
            }
        }

        private void OnNewQuote(Quote quote)
        {
            List<IUser> subscribedUsers = null;

            lock (_subscribedSymbols)
            {
                if (_subscribedSymbols.TryGetValue(quote.Symbol, out subscribedUsers))
                {
                    var response = new NewQuoteResponse();
                    response.Quote = quote;
                    subscribedUsers.ForEach(subscribedUser =>
                    {
                        subscribedUser.SendMessage(JsonConvert.SerializeObject(response));
                    });
                }
            }
        }

        private void OnOrderHistory(Order order)
        {
            lock (_usersOrders)
            {
                if (_usersOrders.ContainsKey(order))
                {
                    var user = _usersOrders[order];

                    user.SendMessage(JsonConvert.SerializeObject(order));
                }
            }
        }
        #endregion //Event handlers

        #region Private methods
        private void HandleSubscribeRequest(SubscribeRequest request)
        {
            var userSession = request.User.Session as SuperWebSocket.WebSocketSession;
            List<IUser> subscribedUsers = null;

            lock (_subscribedSymbols)
            {
                if (_subscribedSymbols.TryGetValue(request.Symbol, out subscribedUsers))
                {
                    try
                    {
                        if (!subscribedUsers.Contains(request.User))
                            subscribedUsers.Add(request.User);
                    }
                    catch (Exception ex)
                    {
                    }
                }
                else
                {
                    try
                    {
                        subscribedUsers = new List<IUser>();
                        subscribedUsers.Add(request.User);
                        _subscribedSymbols.Add(request.Symbol, subscribedUsers);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }

        private void HandleUnsubscribeRequest(UnsubscribeRequest request)
        {
            List<IUser> subscribedUsers = null;

            lock (_subscribedSymbols)
            {
                if (_subscribedSymbols.TryGetValue(request.Symbol, out subscribedUsers))
                {
                    subscribedUsers.Remove(request.User);

                    if (subscribedUsers == null || subscribedUsers.Count == 0)
                        _subscribedSymbols.Remove(request.Symbol);
                }
            }
        }

        //ToDo implement
        private void HandlePlaceOrderRequest(PlaceOrderRequest request)
        {
            var order = new Order();

            order.CustomId = request.CustomId;
            order.Symbol = request.Symbol;
            order.Price = request.Price;
            order.Quantity = request.Quantity;

            Common.Enums.Side orderSide;
            if (Enum.TryParse<Common.Enums.Side>(request.Side.ToString(), out orderSide))
                order.Side = orderSide;
            else
                throw new ArgumentOutOfRangeException("Side is not valid for order.");

            Common.Enums.OrderType orderType;
            if (Enum.TryParse<Common.Enums.OrderType>(request.Type.ToString(), out orderType))
                order.Type = orderType;
            else
                throw new ArgumentOutOfRangeException("Type is not valid for order.");

            _fixManager.PlaceOrder(order);
        }
        #endregion //Private methods

        #region IDisposable Members
        public override void Dispose()
        {
            ServerBase.NewMessageReceived -= OnNewMessageReceived;
            ServerBase.SessionClosed -= OnSessionClosed;

            _fixManager.OnNewQuote -= OnNewQuote;
            _fixManager.OnOrderExecutionReport -= OnOrderHistory;

            _fixManager.Dispose();

            base.Dispose();
        }
        #endregion
    }
}
