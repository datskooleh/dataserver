﻿using Common.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestClient.Classes
{
    public class Market
    {
        public String ExchangeName { get; set; }

        public List<MarketDay> Schedule { get; set; }
    }
}
