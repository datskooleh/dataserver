﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Classes
{
    //for use in schedule
    public class MarketDay : ReportDay
    {
        public Int32 Duration { get; set; }
    }
}
