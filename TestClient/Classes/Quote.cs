﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Classes
{
    public class Quote
    {
        public String Symbol;
        public DateTime Date;
        public Double Bid;
        public Double BidSize;
        public Double Ask;
        public Double AskSize;
        public Double Price;
        public Double Volume;

        public Quote()
        {
            Symbol = String.Empty;
            Date = default(DateTime);
            Bid = default(Double);
            BidSize = default(Double);
            Ask = default(Double);
            AskSize = default(Double);
            Price = default(Double);
            Volume = default(Double);
        }

        public override Boolean Equals(object obj)
        {
            var result = obj == null;

            if (!result)
            {
                var casted = obj as Quote;

                result = casted == null;

                if (!result)
                    result = Equals(casted);
            }

            return result;
        }

        private Boolean Equals(Quote quote)
        {
            return quote.Symbol.Equals(Symbol);
        }

        public override Int32 GetHashCode()
        {
            var hash = 23;

            return hash * 7 * (!String.IsNullOrWhiteSpace(Symbol) ? Symbol.GetHashCode() : 0) +
                hash * 7 * Date.GetHashCode() +
                hash * 7 * Bid.GetHashCode() +
                hash * 7 * BidSize.GetHashCode() +
                hash * 7 * Ask.GetHashCode() +
                hash * 7 * AskSize.GetHashCode() +
                hash * 7 * Price.GetHashCode() +
                hash * 7 * Volume.GetHashCode();
        }
    }
}
