﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Classes
{
    public class ReportDay
    {
        public String Day { get; set; }

        public String Start { get; set; }
    }
}
