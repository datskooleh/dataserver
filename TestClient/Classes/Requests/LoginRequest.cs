﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonServer.Classes.Requests
{
    [DataContract]
    public sealed class LoginRequest : Request
    {
        public LoginRequest(String login, String password)
        {
            Login = login;
            Password = password;
        }

        public LoginRequest() : this(String.Empty, String.Empty)
        {
        }

        [DataMember]
        public String Login { get; set; }

        [DataMember]
        public String Password { get; set; }
    }
}
