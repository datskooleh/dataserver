﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonServer.Classes.Requests
{
    [DataContract]
    
    public sealed class MarketsGetRequest : Request
    {
        public MarketsGetRequest(String exchangeName)
        {
        }

        public MarketsGetRequest() : this(String.Empty)
        {
        }

        [DataMember]
        public String ExchangeName { get; set; }
    }
}
