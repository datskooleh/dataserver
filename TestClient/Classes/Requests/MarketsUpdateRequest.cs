﻿using Common.Classes;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonServer.Classes.Requests
{
    [DataContract]
    
    public sealed class MarketsUpdateRequest : Request
    {
        public MarketsUpdateRequest(String exchangeName, List<MarketDay> schedule)
        {
            ExchangeName = exchangeName;
            Schedule = schedule;
        }

        [DataMember]
        public String ExchangeName { get; set; }

        [DataMember]
        public List<MarketDay> Schedule { get; set; }
    }
}
