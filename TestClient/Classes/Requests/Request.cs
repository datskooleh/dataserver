﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonServer.Classes.Requests
{
    [DataContract]
    [KnownType(typeof(LoginRequest))]
    [KnownType(typeof(LogoutRequest))]
    [KnownType(typeof(MarketsAddRequest))]
    [KnownType(typeof(MarketsGetRequest))]
    [KnownType(typeof(MarketsUpdateRequest))]
    [KnownType(typeof(ReportAddRequest))]
    [KnownType(typeof(ReportDeleteRequest))]
    [KnownType(typeof(ReportsGetRequest))]
    [KnownType(typeof(ReportUpdateRequest))]
    [KnownType(typeof(UsersGetRequest))]
    [KnownType(typeof(UserUpdateRequest))]
    [KnownType(typeof(SubscribeRequest))]
    [KnownType(typeof(UnsubscribeRequest))]
    [KnownType(typeof(PlaceOrderRequest))]
    public class Request
    {
        [JsonProperty]
        public string MsgType { get; set; }

        public Request()
        {
            this.MsgType = this.GetType().Name;
        }
    }
}
