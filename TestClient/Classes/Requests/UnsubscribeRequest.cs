﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace CommonServer.Classes.Requests
{
    [DataContract]
    public class UnsubscribeRequest : Request
    {
        [DataMember]
        public String Symbol { get; set; }
    }
}
