﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonServer.Classes.Requests
{
    public class UsersUpdateRequest
    {
        public String UserLogin { get; set; }

        public String UserMaxTradesNumber { get; set; } //int

        public String BalancePointToDollarRatio; //double

        public String BackstopPoints; // int;

        public String AutoCompoundStatus; //byte: 1 or 0

        public String AutoCompoundMultiptier; //double
    }
}
