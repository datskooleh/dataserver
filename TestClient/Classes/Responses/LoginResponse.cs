﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonServer.Classes.Responses
{
    [DataContract]
    public sealed class LoginResponse : Response
    {
        [DataMember]
        public String Message { get; set; }
    }
}
