﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace CommonServer.Classes.Responses
{
    [DataContract]
    public class NewQuoteResponse : Response
    {
        [DataMember]
        public Common.Classes.Quote Quote { get; set; }
    }
}
