﻿using Common.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonServer.Classes.Responses
{
    [DataContract]
    public sealed class ReportsGetResponse : Response
    {
        [DataMember]
        public Dictionary<String, List<ReportDay>> Reports { get; set; }

        [DataMember]
        public String Message { get; set; }
    }
}
