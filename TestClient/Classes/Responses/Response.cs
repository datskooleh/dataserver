﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonServer.Classes.Responses
{
    [DataContract]
    [KnownType(typeof(LoginResponse))]
    [KnownType(typeof(LogoutResponse))]
    [KnownType(typeof(MarketResponse))]
    [KnownType(typeof(MarketsGetResponse))]
    [KnownType(typeof(ReportResponse))]
    [KnownType(typeof(ReportsGetResponse))]
    [KnownType(typeof(UserResponse))]
    [KnownType(typeof(UsersGetResponse))]
    [KnownType(typeof(NewQuoteResponse))]
    [KnownType(typeof(PlaceOrderResponse))]
    public class Response
    {
        [JsonProperty]
        public string MsgType { get; set; }

        public Response()
        {
            this.MsgType = this.GetType().Name;
        }
    }
}
