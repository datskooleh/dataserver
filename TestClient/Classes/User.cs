﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Classes
{
    public class User
    {
        public String Login { get; set; }

        public Int32 MaxTradesNumber { get; set; } //int

        public Double BalancePointToDollarRatio; //double

        public Int32 BackstopPoints; // int;

        public Byte AutoCompoundStatus; //byte: 1 or 0

        public Double AutoCompoundMultiptier; //double

        public User()
        {
            MaxTradesNumber = 2;
            BalancePointToDollarRatio = 1.0;
            BackstopPoints = 10;
            AutoCompoundStatus = 0;
            AutoCompoundMultiptier = 0;
        }
    }
}
