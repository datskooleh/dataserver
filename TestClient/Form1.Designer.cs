﻿namespace TestClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConnect = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbHost = new System.Windows.Forms.TextBox();
            this.tbLogin = new System.Windows.Forms.TextBox();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnMarketsGet = new System.Windows.Forms.Button();
            this.btnMarketsUpdate = new System.Windows.Forms.Button();
            this.dgvMarkets = new System.Windows.Forms.DataGridView();
            this.marketExchangeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.marketSchedule = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnMarketsAdd = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.cbSymbols = new System.Windows.Forms.ComboBox();
            this.btnUnsubscribe = new System.Windows.Forms.Button();
            this.dgvSubscribes = new System.Windows.Forms.DataGridView();
            this.columnSubscribesSymbol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnSubscribesDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnSubscribesAsk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnSubscribesAskSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnSubscribesBid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnSubscribesBidSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnSubscribesVolume = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnSubscribesPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSubscribe = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dgvOrders = new System.Windows.Forms.DataGridView();
            this.btnOrderPlace = new System.Windows.Forms.Button();
            this.btnOrderEdit = new System.Windows.Forms.Button();
            this.btnOrderDelete = new System.Windows.Forms.Button();
            this.columnOrderTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnOrderSymbol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnOrderType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnOrderSide = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnOrderQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMarkets)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSubscribes)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(6, 42);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(156, 23);
            this.btnConnect.TabIndex = 0;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Login";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Host";
            // 
            // tbHost
            // 
            this.tbHost.Location = new System.Drawing.Point(38, 16);
            this.tbHost.Name = "tbHost";
            this.tbHost.Size = new System.Drawing.Size(124, 20);
            this.tbHost.TabIndex = 3;
            this.tbHost.Text = "127.0.0.1:2012";
            // 
            // tbLogin
            // 
            this.tbLogin.Location = new System.Drawing.Point(62, 19);
            this.tbLogin.Name = "tbLogin";
            this.tbLogin.Size = new System.Drawing.Size(100, 20);
            this.tbLogin.TabIndex = 4;
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(62, 45);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(100, 20);
            this.tbPassword.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Password";
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(6, 71);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(156, 23);
            this.btnLogin.TabIndex = 7;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnConnect);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbHost);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(168, 72);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Connection";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnLogin);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.tbLogin);
            this.groupBox2.Controls.Add(this.tbPassword);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(12, 90);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(169, 100);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Account";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(187, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(480, 383);
            this.tabControl1.TabIndex = 10;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnMarketsGet);
            this.tabPage1.Controls.Add(this.btnMarketsUpdate);
            this.tabPage1.Controls.Add(this.dgvMarkets);
            this.tabPage1.Controls.Add(this.btnMarketsAdd);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(472, 357);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Markets";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnMarketsGet
            // 
            this.btnMarketsGet.Location = new System.Drawing.Point(168, 7);
            this.btnMarketsGet.Name = "btnMarketsGet";
            this.btnMarketsGet.Size = new System.Drawing.Size(75, 23);
            this.btnMarketsGet.TabIndex = 3;
            this.btnMarketsGet.Text = "Get";
            this.btnMarketsGet.UseVisualStyleBackColor = true;
            // 
            // btnMarketsUpdate
            // 
            this.btnMarketsUpdate.Location = new System.Drawing.Point(87, 7);
            this.btnMarketsUpdate.Name = "btnMarketsUpdate";
            this.btnMarketsUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnMarketsUpdate.TabIndex = 2;
            this.btnMarketsUpdate.Text = "Update";
            this.btnMarketsUpdate.UseVisualStyleBackColor = true;
            this.btnMarketsUpdate.Click += new System.EventHandler(this.btnMarketsUpdate_Click);
            // 
            // dgvMarkets
            // 
            this.dgvMarkets.AllowUserToAddRows = false;
            this.dgvMarkets.AllowUserToDeleteRows = false;
            this.dgvMarkets.AllowUserToResizeColumns = false;
            this.dgvMarkets.AllowUserToResizeRows = false;
            this.dgvMarkets.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMarkets.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvMarkets.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvMarkets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMarkets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.marketExchangeName,
            this.marketSchedule});
            this.dgvMarkets.Location = new System.Drawing.Point(3, 36);
            this.dgvMarkets.MultiSelect = false;
            this.dgvMarkets.Name = "dgvMarkets";
            this.dgvMarkets.RowHeadersVisible = false;
            this.dgvMarkets.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMarkets.Size = new System.Drawing.Size(466, 318);
            this.dgvMarkets.TabIndex = 1;
            // 
            // marketExchangeName
            // 
            this.marketExchangeName.HeaderText = "Exchange";
            this.marketExchangeName.Name = "marketExchangeName";
            this.marketExchangeName.ReadOnly = true;
            this.marketExchangeName.Width = 80;
            // 
            // marketSchedule
            // 
            this.marketSchedule.HeaderText = "Schedule";
            this.marketSchedule.Name = "marketSchedule";
            this.marketSchedule.ReadOnly = true;
            this.marketSchedule.Width = 77;
            // 
            // btnMarketsAdd
            // 
            this.btnMarketsAdd.Location = new System.Drawing.Point(6, 7);
            this.btnMarketsAdd.Name = "btnMarketsAdd";
            this.btnMarketsAdd.Size = new System.Drawing.Size(75, 23);
            this.btnMarketsAdd.TabIndex = 0;
            this.btnMarketsAdd.Text = "Add";
            this.btnMarketsAdd.UseVisualStyleBackColor = true;
            this.btnMarketsAdd.Click += new System.EventHandler(this.btnMarketsAdd_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(472, 357);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Reports";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(472, 357);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Users";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.cbSymbols);
            this.tabPage4.Controls.Add(this.btnUnsubscribe);
            this.tabPage4.Controls.Add(this.dgvSubscribes);
            this.tabPage4.Controls.Add(this.label4);
            this.tabPage4.Controls.Add(this.btnSubscribe);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(472, 357);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Subscribes";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // cbSymbols
            // 
            this.cbSymbols.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSymbols.FormattingEnabled = true;
            this.cbSymbols.Location = new System.Drawing.Point(50, 3);
            this.cbSymbols.Name = "cbSymbols";
            this.cbSymbols.Size = new System.Drawing.Size(121, 21);
            this.cbSymbols.TabIndex = 15;
            // 
            // btnUnsubscribe
            // 
            this.btnUnsubscribe.Location = new System.Drawing.Point(394, 1);
            this.btnUnsubscribe.Name = "btnUnsubscribe";
            this.btnUnsubscribe.Size = new System.Drawing.Size(75, 23);
            this.btnUnsubscribe.TabIndex = 14;
            this.btnUnsubscribe.Text = "Unsubscribe";
            this.btnUnsubscribe.UseVisualStyleBackColor = true;
            this.btnUnsubscribe.Click += new System.EventHandler(this.btnUnsubscribe_Click);
            // 
            // dgvSubscribes
            // 
            this.dgvSubscribes.AllowUserToAddRows = false;
            this.dgvSubscribes.AllowUserToDeleteRows = false;
            this.dgvSubscribes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSubscribes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSubscribes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnSubscribesSymbol,
            this.columnSubscribesDate,
            this.columnSubscribesAsk,
            this.columnSubscribesAskSize,
            this.columnSubscribesBid,
            this.columnSubscribesBidSize,
            this.columnSubscribesVolume,
            this.columnSubscribesPrice});
            this.dgvSubscribes.Location = new System.Drawing.Point(3, 30);
            this.dgvSubscribes.MultiSelect = false;
            this.dgvSubscribes.Name = "dgvSubscribes";
            this.dgvSubscribes.RowHeadersVisible = false;
            this.dgvSubscribes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSubscribes.Size = new System.Drawing.Size(466, 324);
            this.dgvSubscribes.TabIndex = 13;
            this.dgvSubscribes.VirtualMode = true;
            // 
            // columnSubscribesSymbol
            // 
            this.columnSubscribesSymbol.HeaderText = "Symbol";
            this.columnSubscribesSymbol.Name = "columnSubscribesSymbol";
            this.columnSubscribesSymbol.ReadOnly = true;
            // 
            // columnSubscribesDate
            // 
            this.columnSubscribesDate.HeaderText = "Date";
            this.columnSubscribesDate.Name = "columnSubscribesDate";
            this.columnSubscribesDate.ReadOnly = true;
            // 
            // columnSubscribesAsk
            // 
            this.columnSubscribesAsk.HeaderText = "Ask";
            this.columnSubscribesAsk.Name = "columnSubscribesAsk";
            this.columnSubscribesAsk.ReadOnly = true;
            // 
            // columnSubscribesAskSize
            // 
            this.columnSubscribesAskSize.HeaderText = "Ask Size";
            this.columnSubscribesAskSize.Name = "columnSubscribesAskSize";
            this.columnSubscribesAskSize.ReadOnly = true;
            // 
            // columnSubscribesBid
            // 
            this.columnSubscribesBid.HeaderText = "Bid";
            this.columnSubscribesBid.Name = "columnSubscribesBid";
            this.columnSubscribesBid.ReadOnly = true;
            // 
            // columnSubscribesBidSize
            // 
            this.columnSubscribesBidSize.HeaderText = "Bid Size";
            this.columnSubscribesBidSize.Name = "columnSubscribesBidSize";
            this.columnSubscribesBidSize.ReadOnly = true;
            // 
            // columnSubscribesVolume
            // 
            this.columnSubscribesVolume.HeaderText = "Volume";
            this.columnSubscribesVolume.Name = "columnSubscribesVolume";
            this.columnSubscribesVolume.ReadOnly = true;
            // 
            // columnSubscribesPrice
            // 
            this.columnSubscribesPrice.HeaderText = "Price";
            this.columnSubscribesPrice.Name = "columnSubscribesPrice";
            this.columnSubscribesPrice.ReadOnly = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Symbol";
            // 
            // btnSubscribe
            // 
            this.btnSubscribe.Location = new System.Drawing.Point(177, 1);
            this.btnSubscribe.Name = "btnSubscribe";
            this.btnSubscribe.Size = new System.Drawing.Size(75, 23);
            this.btnSubscribe.TabIndex = 0;
            this.btnSubscribe.Text = "Subscribe";
            this.btnSubscribe.UseVisualStyleBackColor = true;
            this.btnSubscribe.Click += new System.EventHandler(this.btnSubscribe_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.btnOrderDelete);
            this.tabPage5.Controls.Add(this.btnOrderEdit);
            this.tabPage5.Controls.Add(this.btnOrderPlace);
            this.tabPage5.Controls.Add(this.dgvOrders);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(472, 357);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Orders";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // dgvOrders
            // 
            this.dgvOrders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOrders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnOrderTime,
            this.columnOrderSymbol,
            this.columnOrderType,
            this.columnOrderSide,
            this.columnOrderQuantity});
            this.dgvOrders.Location = new System.Drawing.Point(3, 32);
            this.dgvOrders.Name = "dgvOrders";
            this.dgvOrders.RowHeadersVisible = false;
            this.dgvOrders.Size = new System.Drawing.Size(466, 322);
            this.dgvOrders.TabIndex = 0;
            // 
            // btnOrderPlace
            // 
            this.btnOrderPlace.Location = new System.Drawing.Point(3, 3);
            this.btnOrderPlace.Name = "btnOrderPlace";
            this.btnOrderPlace.Size = new System.Drawing.Size(75, 23);
            this.btnOrderPlace.TabIndex = 1;
            this.btnOrderPlace.Text = "New order";
            this.btnOrderPlace.UseVisualStyleBackColor = true;
            this.btnOrderPlace.Click += new System.EventHandler(this.btnOrderPlace_Click);
            // 
            // btnOrderEdit
            // 
            this.btnOrderEdit.Location = new System.Drawing.Point(84, 3);
            this.btnOrderEdit.Name = "btnOrderEdit";
            this.btnOrderEdit.Size = new System.Drawing.Size(75, 23);
            this.btnOrderEdit.TabIndex = 2;
            this.btnOrderEdit.Text = "Edit order";
            this.btnOrderEdit.UseVisualStyleBackColor = true;
            this.btnOrderEdit.Click += new System.EventHandler(this.btnOrderEdit_Click);
            // 
            // btnOrderDelete
            // 
            this.btnOrderDelete.Location = new System.Drawing.Point(165, 3);
            this.btnOrderDelete.Name = "btnOrderDelete";
            this.btnOrderDelete.Size = new System.Drawing.Size(75, 23);
            this.btnOrderDelete.TabIndex = 3;
            this.btnOrderDelete.Text = "Delete order";
            this.btnOrderDelete.UseVisualStyleBackColor = true;
            this.btnOrderDelete.Click += new System.EventHandler(this.btnOrderDelete_Click);
            // 
            // columnOrderTime
            // 
            this.columnOrderTime.HeaderText = "Time";
            this.columnOrderTime.Name = "columnOrderTime";
            this.columnOrderTime.ReadOnly = true;
            // 
            // columnOrderSymbol
            // 
            this.columnOrderSymbol.HeaderText = "Symbol";
            this.columnOrderSymbol.Name = "columnOrderSymbol";
            this.columnOrderSymbol.ReadOnly = true;
            // 
            // columnOrderType
            // 
            this.columnOrderType.HeaderText = "Type";
            this.columnOrderType.Name = "columnOrderType";
            this.columnOrderType.ReadOnly = true;
            // 
            // columnOrderSide
            // 
            this.columnOrderSide.HeaderText = "Side";
            this.columnOrderSide.Name = "columnOrderSide";
            this.columnOrderSide.ReadOnly = true;
            // 
            // columnOrderQuantity
            // 
            this.columnOrderQuantity.HeaderText = "Quantity";
            this.columnOrderQuantity.Name = "columnOrderQuantity";
            this.columnOrderQuantity.ReadOnly = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 408);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMarkets)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSubscribes)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrders)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbHost;
        private System.Windows.Forms.TextBox tbLogin;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnMarketsAdd;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dgvMarkets;
        private System.Windows.Forms.DataGridViewTextBoxColumn marketExchangeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn marketSchedule;
        private System.Windows.Forms.Button btnMarketsUpdate;
        private System.Windows.Forms.Button btnMarketsGet;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSubscribe;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView dgvSubscribes;
        private System.Windows.Forms.Button btnUnsubscribe;
        private System.Windows.Forms.ComboBox cbSymbols;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnSubscribesSymbol;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnSubscribesDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnSubscribesAsk;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnSubscribesAskSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnSubscribesBid;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnSubscribesBidSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnSubscribesVolume;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnSubscribesPrice;
        private System.Windows.Forms.DataGridView dgvOrders;
        private System.Windows.Forms.Button btnOrderDelete;
        private System.Windows.Forms.Button btnOrderEdit;
        private System.Windows.Forms.Button btnOrderPlace;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnOrderTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnOrderSymbol;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnOrderType;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnOrderSide;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnOrderQuantity;
    }
}

