﻿using Common.Classes;
using CommonServer.Classes.Requests;
using CommonServer.Classes.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestClient
{
    public partial class Form1 : Form, IDisposable
    {
        private object _locker = new object();

        WebSocket4Net.WebSocket _clientSocket;

        private List<Quote> _subscribedSymbols = new List<Quote>();
        private List<Order> _orders = new List<Order>();

        private bool _isLogin = true;

        public Form1()
        {
            InitializeComponent();


            tabControl1.Enabled = false;

            dgvSubscribes.CellValueNeeded += OnCellValueNeeded;

            new List<String>()
            {
                "EUR/USD",
                //"UAH/USD",
                //"CAD/USD",
                //"CHF/USD",
                //"GBP/USD",
                //"HRK/USD",
                //"LVL/USD",
                //"LTL/USD",
            }.ForEach(symbol =>
            {
                cbSymbols.Items.Add(symbol);
            });
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
                this.Invoke(new MethodInvoker(() => btnConnect_Click(sender, e)));
            else
            {
                btnConnect.Enabled = false;

                _clientSocket = new WebSocket4Net.WebSocket("ws://" + tbHost.Text);

                _clientSocket.Opened += OnSocketOpened;
                _clientSocket.Closed += OnSocketClosed;
                _clientSocket.MessageReceived += OnMessageReceived;
                _clientSocket.Error += OnError;

                _clientSocket.Open();
            }
        }

        private void OnMessageReceived(object sender, WebSocket4Net.MessageReceivedEventArgs e)
        {
            if (this.InvokeRequired)
                this.Invoke(new MethodInvoker(() => OnMessageReceived(sender, e)));
            else
            {
                lock (_locker)
                {
                    Response parsedMessage = null;

                    try
                    {
                        parsedMessage = JsonConvert.DeserializeObject<Response>(e.Message);

                        if (parsedMessage.MsgType == typeof(LoginResponse).Name)
                        {
                            LoginResponse response = null;
                            try
                            {
                                response = JsonConvert.DeserializeObject<LoginResponse>(e.Message);
                                OnLoginResponse(response);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Login message response error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else if (parsedMessage.MsgType == typeof(LogoutResponse).Name)
                        {
                            LogoutResponse response = null;
                            try
                            {
                                response = JsonConvert.DeserializeObject<LogoutResponse>(e.Message);
                                OnLogoutResponse(response);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Logout message response error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else if (parsedMessage.MsgType == typeof(NewQuoteResponse).Name)
                        {
                            NewQuoteResponse response = null;
                            try
                            {
                                response = JsonConvert.DeserializeObject<NewQuoteResponse>(e.Message);
                                OnNewQuoteResponse(response);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Server message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error when parsing message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        #region WebSocket Event handlers
        private void OnError(object sender, SuperSocket.ClientEngine.ErrorEventArgs e)
        {
            if (this.InvokeRequired)
                Invoke(new MethodInvoker(() => OnError(sender, e)));
            else
            {

                btnConnect.Enabled = true;

                tbLogin.Enabled = true;
                tbPassword.Enabled = true;
                btnLogin.Text = "Login";

                MessageBox.Show(e.Exception.Message);
            }
        }

        private void OnSocketClosed(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
                this.Invoke(new MethodInvoker(() => OnSocketClosed(sender, e)));
            else
            {
                btnConnect.Enabled = true;


                tbLogin.Enabled = true;
                tbPassword.Enabled = true;
                btnLogin.Text = "Login";
            }
        }

        private void OnSocketOpened(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
                this.Invoke(new MethodInvoker(() => OnSocketOpened(sender, e)));
            else
            {
                btnConnect.Enabled = false;

            }
        }
        #endregion //WebSocket Event handlers

        #region Responses
        private void OnLoginResponse(LoginResponse response)
        {
            if (this.InvokeRequired)
                this.Invoke(new MethodInvoker(() => OnLoginResponse(response)));
            else
            {
                if (String.IsNullOrWhiteSpace(response.Message))
                {
                    btnLogin.Text = "Logout";

                    tbLogin.Enabled = false;
                    tbPassword.Enabled = false;
                    tabControl1.Enabled = true;
                    _isLogin = false;
                }
                else
                    MessageBox.Show(response.Message, "Login result", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OnLogoutResponse(LogoutResponse response)
        {
            btnLogin.Text = "Login";

            tbLogin.Enabled = true;
            tbPassword.Enabled = true;
            tabControl1.Enabled = false;
            _isLogin = true;
            MessageBox.Show(response.Message, "Logout result", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void OnNewQuoteResponse(NewQuoteResponse response)
        {
            lock (_subscribedSymbols)
            {
                var quoteItem = _subscribedSymbols.SingleOrDefault(x => x.Equals(response.Quote));

                if (quoteItem == null)
                    return;

                quoteItem.Ask = response.Quote.Ask;
                quoteItem.AskSize = response.Quote.AskSize;
                quoteItem.Bid = response.Quote.Bid;
                quoteItem.BidSize = response.Quote.BidSize;
                quoteItem.Date = response.Quote.Date;
                quoteItem.Price = response.Quote.Price;
                quoteItem.Volume = response.Quote.Volume;

                dgvSubscribes.Refresh();
            }
        }
        #endregion //Responses

        #region Forms event handlers
        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (_isLogin)
            {
                var loginRequest = new LoginRequest();
                loginRequest.Login = tbLogin.Text;
                loginRequest.Password = tbPassword.Text;

                var jsonString = JsonConvert.SerializeObject(loginRequest);

                _clientSocket.Send(jsonString);
            }
            else
            {
                var logoutRequest = new LogoutRequest();

                var jsonString = JsonConvert.SerializeObject(logoutRequest);

                _clientSocket.Send(jsonString);
            }
        }

        private void btnMarketsAdd_Click(object sender, EventArgs e)
        {
            var request = new MarketsAddRequest();
            request.ExchangeName = "test";
            request.Schedule = new List<MarketDay>
            {
                new MarketDay()
                {
                    Day = "Mon",
                    Start = "16:00",
                    Duration = 360
                },
                new MarketDay()
                {
                    Day = "",
                    Start = "16:00",
                    Duration = 360
                },
                new MarketDay()
                {
                    Start = "16:00",
                    Duration = 360
                },
            };

            _clientSocket.Send(JsonConvert.SerializeObject(request));

            //var market = new MarketForm();
            //market.Owner = this;

            //market.ShowDialog();

            //if (market.DialogResult == System.Windows.Forms.DialogResult.OK)
            //{
            //}
        }

        private void btnMarketsUpdate_Click(object sender, EventArgs e)
        {
            //var market = new MarketForm();
            //market.Owner = this;

            //market.ShowDialog();

            //if (market.DialogResult == System.Windows.Forms.DialogResult.OK)
            //{
            //}
        }

        private void btnSubscribe_Click(object sender, EventArgs e)
        {
            if (cbSymbols.SelectedIndex != -1)
            {
                var request = new SubscribeRequest();
                request.Symbol = cbSymbols.SelectedItem.ToString();

                var quote = new Quote();
                quote.Symbol = request.Symbol;

                if (!_subscribedSymbols.Any(x => x.Equals(quote)))
                {
                    _subscribedSymbols.Add(quote);
                    dgvSubscribes.RowCount = _subscribedSymbols.Count;
                    dgvSubscribes.Refresh();
                }

                _clientSocket.Send(JsonConvert.SerializeObject(request));
            }
        }

        private void btnUnsubscribe_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = null;
            String symbol = String.Empty;

            lock (dgvSubscribes)
            {
                if (dgvSubscribes.SelectedRows == null || dgvSubscribes.SelectedRows.Count == 0)
                    return;

                row = dgvSubscribes.SelectedRows[0];
                symbol = row.Cells[0].Value.ToString();
            }

            lock (_subscribedSymbols)
            {
                var request = new UnsubscribeRequest();
                request.Symbol = symbol;

                _clientSocket.Send(JsonConvert.SerializeObject(request));

                var quoteItem = _subscribedSymbols.SingleOrDefault(x => x.Symbol.Equals(symbol));

                if (quoteItem != null)
                {
                    _subscribedSymbols.Remove(quoteItem);
                    dgvSubscribes.RowCount = _subscribedSymbols.Count;
                    dgvSubscribes.Refresh();
                }
            }
        }

        private void OnCellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            lock (_subscribedSymbols)
            {
                if (e.RowIndex < 0 || e.RowIndex >= _subscribedSymbols.Count)
                    return;

                var quote = _subscribedSymbols[e.RowIndex];

                if (e.ColumnIndex == 0)
                    e.Value = quote.Symbol;
                else if (e.ColumnIndex == 1)
                    e.Value = quote.Date.ToLocalTime();
                else if (e.ColumnIndex == 2)
                    e.Value = quote.Ask;
                else if (e.ColumnIndex == 3)
                    e.Value = quote.AskSize;
                else if (e.ColumnIndex == 4)
                    e.Value = quote.Bid;
                else if (e.ColumnIndex == 5)
                    e.Value = quote.BidSize;
                else if (e.ColumnIndex == 6)
                    e.Value = quote.Volume;
                else if (e.ColumnIndex == 7)
                    e.Value = quote.Price;
            }
        }
        #endregion //Forms event hadlers

        #region IDisposable Members
        void IDisposable.Dispose()
        {
            _clientSocket.Close();
        }
        #endregion

        private void btnOrderPlace_Click(object sender, EventArgs e)
        {
            var orderForm = new OrderForm();
            var formResult = orderForm.ShowDialog();

            if (formResult == System.Windows.Forms.DialogResult.OK)
            {
                var order = orderForm.GetOrder();

                _orders.Add(order);

                var request = new PlaceOrderRequest();
                request.CustomId = order.CustomId;
                request.Symbol = order.Symbol;
                request.Type = (Int32)order.Type;
                request.Side = (Int32)order.Side;
                request.Quantity = order.Quantity;
                request.Price = order.Price;

                _clientSocket.Send(JsonConvert.SerializeObject(request));
            }
        }

        private void btnOrderEdit_Click(object sender, EventArgs e)
        {
            if (dgvOrders.SelectedRows == null || dgvOrders.SelectedRows.Count == 0)
                return;

            var orderForm = new OrderForm();
            var order = _orders[dgvOrders.SelectedRows[0].Index - 1];
            orderForm.InitOrder(order);

            var formResult = orderForm.ShowDialog();

            if (formResult == System.Windows.Forms.DialogResult.OK)
            {
                var editedOrder = orderForm.GetOrder();

                order.CustomId = editedOrder.CustomId;
                order.Symbol = editedOrder.Symbol;
                order.Type = editedOrder.Type;
                order.Side = editedOrder.Side;
                order.Quantity = editedOrder.Quantity;
                order.Price = editedOrder.Price;

                var request = new PlaceOrderRequest();
                request.CustomId = order.CustomId;
                request.Symbol = order.Symbol;
                request.Type = (Int32)order.Type;
                request.Side = (Int32)order.Side;
                request.Quantity = order.Quantity;
                request.Price = order.Price;

                _clientSocket.Send(JsonConvert.SerializeObject(request));
            }
        }

        private void btnOrderDelete_Click(object sender, EventArgs e)
        {
            if (dgvOrders.SelectedRows == null || dgvOrders.SelectedRows.Count == 0)
                return;

            var orderId = dgvOrders.SelectedRows[0].Tag as String;

            _orders.RemoveAt(dgvOrders.SelectedRows[0].Index - 1);
        }
    }
}
