﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TestClient.Classes;

namespace TestClient
{
    public partial class MarketForm : Form
    {
        public MarketForm()
        {
            InitializeComponent();
        }

        public Market GetMarket()
        {
            var market = new Market();
            market.ExchangeName = tbExchangeName.Text;
            market.Schedule = new List<Common.Classes.MarketDay>();

            foreach (var item in lvSchedule.Items)
            {
                var marketDay = new Common.Classes.MarketDay();

                market.Schedule.Add(marketDay);
            }

            return market;
        }
    }
}
