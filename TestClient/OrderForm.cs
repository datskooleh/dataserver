﻿using Common.Classes;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestClient
{
    public partial class OrderForm : Form
    {
        private String _orderId = String.Empty;

        public OrderForm()
        {
            InitializeComponent();

            cbSide.Items.AddRange(Enum.GetNames(typeof(Side)));
            cbType.Items.AddRange(Enum.GetNames(typeof(OrderType)));
        }

        public Order GetOrder()
        {
            var order = new Common.Classes.Order();

            if (String.IsNullOrWhiteSpace(_orderId))
                order.CustomId = Guid.NewGuid().ToString();
            else
                order.CustomId = _orderId;
            order.Symbol = tbSymbol.Text;
            if (!String.IsNullOrWhiteSpace(tbPrice.Text))
                order.Price = Double.Parse(tbPrice.Text);
            order.Quantity = (Int32)nupQuantity.Value;
            order.Side = (Side)Enum.Parse(typeof(Side), cbSide.SelectedItem.ToString());
            order.Type = (OrderType)Enum.Parse(typeof(OrderType), cbType.SelectedItem.ToString());

            return order;
        }

        public void InitOrder(Order order)
        {
            _orderId = order.CustomId;

            tbSymbol.Text = order.Symbol;
            tbPrice.Text = order.Price.ToString();
            nupQuantity.Value = order.Quantity;

            cbSide.SelectedIndex = cbSide.Items.IndexOf(Enum.GetName(typeof(Side), ((Int32)order.Side)));
            cbType.SelectedIndex = cbSide.Items.IndexOf(Enum.GetName(typeof(OrderType), ((Int32)order.Type)));
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
